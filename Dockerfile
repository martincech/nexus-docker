FROM cloudsc/nexus3-blobstore-s3:3.12.1-patch

LABEL maintainer="David Makovsky <makovsky@veit.cz>"
LABEL vendor="VEIT" \
      cz.veit.name="Nexus 3 Repository Manager image with S3 blobstore and SSL"

USER root

ENV SSL_STOREPASS=
ENV SSL_KEYPASS=
ENV SSL_DOMAIN_NAME="artifactory.veit.cz"
ENV SSL_WORK /etc/ssl/private

# configure nexus runtime
ENV SONATYPE_DIR=/opt/sonatype
ENV NEXUS_HOME=${SONATYPE_DIR}/nexus
ENV NEXUS_DATA=/nexus-data
ENV NEXUS_CONTEXT=''
ENV SONATYPE_WORK=${SONATYPE_DIR}/sonatype-work
ENV DOCKER_TYPE='docker'

ENV NEXUS_PROPS_FILE=${SONATYPE_WORK}/nexus3/etc/nexus.properties

#### Configure Nexus for SSL ####
RUN sed -i -e '/nexus-args=/ s/=.*/=${jetty.etc}\/jetty.xml,${jetty.etc}\/jetty-requestlog.xml,${jetty.etc}\/jetty-http.xml,${jetty.etc}\/jetty-https.xml,${jetty.etc}\/jetty-http-redirect-to-https.xml/' ${NEXUS_HOME}/etc/nexus-default.properties \
  && printf "\n# ssl modifications\napplication-port-ssl=8443\n">> ${NEXUS_HOME}/etc/nexus-default.properties \
  && sed -i 's/<Set name="KeyStorePath">.*<\/Set>/<Set name="KeyStorePath">\/etc\/ssl\/private\/nexus.pkcs12<\/Set>/g' /${NEXUS_HOME}/etc/jetty/jetty-https.xml \
  && sed -i 's/<Set name="KeyStorePassword">.*<\/Set>/<Set name="KeyStorePassword">tralalak<\/Set>/g' ${NEXUS_HOME}/etc/jetty/jetty-https.xml \
  && sed -i 's/<Set name="KeyManagerPassword">.*<\/Set>/<Set name="KeyManagerPassword">tralalak<\/Set>/g' ${NEXUS_HOME}/etc/jetty/jetty-https.xml \
  && sed -i 's/<Set name="TrustStorePath">.*<\/Set>/<Set name="TrustStorePath">\/etc\/ssl\/private\/nexus.pkcs12<\/Set>/g' ${NEXUS_HOME}/etc/jetty/jetty-https.xml \
  && sed -i 's/<Set name="TrustStorePassword">.*<\/Set>/<Set name="TrustStorePassword"><\/Set>/g' ${NEXUS_HOME}/etc/jetty/jetty-https.xml

VOLUME ${SSL_WORK}

#### http, https, https for docker group, https for hosted docker hub ####
EXPOSE 8081 8443 5000 5001 18443 18444

USER nexus

ENV INSTALL4J_ADD_VM_PARAMS="-Xms1200m -Xmx1200m -XX:MaxDirectMemorySize=2g -Djava.util.prefs.userRoot=${NEXUS_DATA}/javaprefs"

CMD ["sh", "-c", "${SONATYPE_DIR}/start-nexus-repository-manager.sh"]